package oxagileinterview;

public class Version implements Comparable<Version> {
    private String version;

    public String getVersion() {
        return version;
    }

    public Version(String version) {
        if (version == null) {
            throw new IllegalArgumentException("Version cannot be null!");
        }
        if (!version.matches("[0-9]+(\\.[0-9]+)*")) {
            throw new IllegalArgumentException("Invalid version format!");
        }

        this.version = version;
    }


    @Override
    public int compareTo(Version otherVersion) {
        if (otherVersion == null) {
            return 1;
        }

        String[] currentVersion = version.split("\\.");
        String[] newVersion = otherVersion.getVersion().split("\\.");

        int length = Math.max(currentVersion.length, newVersion.length);

        for (int i = 0; i < length; i++) {
            int currentPart = i < currentVersion.length ? Integer.parseInt(currentVersion[i]) : 0;
            int otherPart = i < newVersion.length ? Integer.parseInt(newVersion[i]) : 0;

            if (currentPart < otherPart) {
                return -1;
            }

            if (currentPart > otherPart) {
                return 1;
            }
        }

        return 0;
    }

    public void checkVersions(Version otherVersion) {
        if (this.compareTo(otherVersion) > 0) {
            System.out.printf("%s > %s\n", this.getVersion(), otherVersion.getVersion());
        }

        if (this.compareTo(otherVersion) < 0) {
            System.out.printf("%s < %s\n", this.getVersion(), otherVersion.getVersion());
        }

        if (this.compareTo(otherVersion) == 0) {
            System.out.printf("%s = %s\n", this.getVersion(), otherVersion.getVersion());
        }



    }

}
