package oxagileinterview;

public class App {
    public static void main(String[] args) {
        Version version1 = new Version("1.0.0");
        Version version2 = new Version("1.1.0");

        version1.checkVersions(version2);
    }
}
