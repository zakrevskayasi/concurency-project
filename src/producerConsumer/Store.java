package producerConsumer;

public class Store {
    int counter = 0;//Счетчик товаров
    final int N = 5;//Максимально допустимое число товаров

    //синхронизированный метод для производителей товаров
    synchronized int put(){
        if(counter <= N){
            counter++;
            System.out.println("Склад имеет " + counter + " товар(ов)");
            return 1; //В случае усешного выполнения возвращаем 1
        }

        return 0;//В случае неуспешного выполнения возвращаем 0
    }

    //синхронизированный метод для покупателей
    synchronized int get(){
        if(counter > 0){ //Если хоть один товар присутствует на складе
            counter--; //Мы его берем
            System.out.println("Склад имеет " + counter + " товар(ов)");
            return 1;
        }

        return 0;
    }
}
