package producerConsumer;

public class Producer extends Thread {
    Store store;
    int product = 5; //Количество произведенных товаров

    public Producer(Store store) {
        this.store = store;
    }

    public void run(){
        try{
            while (product > 0){
                product = product - store.put(); //Кладем один товар на склад и уменьшаем количество произведенных товаров на 1
                System.out.println("Производителю осталось положить на склад " + product + " товар(ов)");
                sleep(100); //Время простоя
            }
        }catch (InterruptedException e){
            System.out.println("Работа производителя прервана!");

        }
    }
}
