package producerConsumerWaitNotify;

public class Store {
    private int product = 0;

    public synchronized void get(){
        while (product < 1){
            try {
                wait(); //Ждем, пока потребитель добавит товары на склад
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        product--;
        System.out.println("Покупатель купил 1 товар");
        System.out.println("Товаров на складе: " + product);
        notify();
    }

    public synchronized void put(){
        while (product >= 3){
            try {
                wait(); //Ждем, пока покупатель купит товар
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        product++;
        System.out.println("Производитель добавил 1 товар");
        System.out.println("Товаров на складе: " + product);
        notify();
    }
}
