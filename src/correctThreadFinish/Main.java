package correctThreadFinish;

public class Main {
    public static void main(String[] args) {
        System.out.println("Main thread started!");
        MyThread t = new MyThread();
        new Thread(t, "MyThread").start();

        try{
            Thread.sleep(1100);
            t.disable();
            Thread.sleep(1000);
        }catch (InterruptedException e){
            System.out.println("Thread is interrupted!");
        }

        System.out.println("Main thread finished!");
    }
}
