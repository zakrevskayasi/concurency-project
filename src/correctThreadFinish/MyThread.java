package correctThreadFinish;

public class MyThread implements Runnable {
    private boolean isActive;

    void disable(){
        isActive = false;
    }

    public MyThread() {
        this.isActive = true;
    }

    @Override
    public void run() {
        System.out.printf("Thread %s is started!", Thread.currentThread().getName());
        int counter = 1;

        while(isActive){
            System.out.println("Loop " + counter++);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Thread is interrupted!");
            }
        }

        System.out.printf("Thread %s is finished!", Thread.currentThread().getName());

    }
}
